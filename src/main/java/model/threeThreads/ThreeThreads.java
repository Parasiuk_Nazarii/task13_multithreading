package model.threeThreads;

public class ThreeThreads {
    private static Object sync = new Object();
    Thread thread1 = new Thread(
            () -> {
                for (int i = 0; i < 10; i++) {
                    System.out.println("I am 1 thread " + i);
                }

            }
    );
    Thread thread2 = new Thread(
            () -> {
                for (int i = 0; i < 10; i++) {
                    System.out.println("I am 2 thread " + i);
                }

            }
    );
    Thread thread3 = new Thread(
            () -> {
                for (int i = 0; i < 10; i++) {
                    System.out.println("I am 3 thread " + i);
                }

            }
    );
    Thread sthread1 = new Thread(
            () -> {
                synchronized (sync) {
                    for (int i = 0; i < 10; i++) {
                        System.out.println("I am 1 thread " + i);
                    }

                }
            }
    );
    Thread sthread2 = new Thread(
            () -> {
                synchronized (sync) {
                    for (int i = 0; i < 10; i++) {
                        System.out.println("I am 2 thread " + i);
                    }

                }
            }
    );
    Thread sthread3 = new Thread(
            () -> {
                synchronized (sync) {
                    for (int i = 0; i < 10; i++) {
                        System.out.println("I am 3 thread " + i);
                    }

                }
            }
    );

    public void runAsync() {
        System.out.println("Without synchronized all random:");
        thread1.start();
        thread2.start();
        thread3.start();
        try {
            thread3.join();
            thread2.join();
            thread1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void runSync() {
        System.out.println("With synchronized all are in the order");
        sthread1.start();
        sthread2.start();
        sthread3.start();
        try {
            sthread3.join();
            sthread2.join();
            sthread1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
