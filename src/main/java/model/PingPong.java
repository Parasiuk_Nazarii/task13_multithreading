package model;


import java.util.Scanner;

public class PingPong {
    private static Object sync = new Object();

    public static void start() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input count");
        long finalTimes = sc.nextLong();
        Thread thread1 = new Thread(
                () -> {
                    synchronized (sync) {
                        for (int i = 0; i < finalTimes; i++) {

                            try {
                                sync.wait();
                            } catch (InterruptedException e) {
                            }
                            cls();
                            System.out.println("ⓞ   ");
                            sync.notify();
                        }

                    }
                }
        );
        Thread thread2 = new Thread(
                () -> {
                    synchronized (sync) {
                        for (int i = 0; i < finalTimes; i++) {
                            sync.notify();
                            try {
                                sync.wait();
                            } catch (InterruptedException e) {
                            }
                            cls();
                            System.out.println("        ⓞ");

                        }

                    }
                }
        );

        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void cls() {
        for (int i = 0; i < 5; i++) {
            System.out.println(" ");
        }
    }
}

