package model.fibonacci;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciExecutor {
    private Fibonacci fib = new Fibonacci(10);

    public void go() {
        Runnable run1 =
                () -> {
                    for (int i = 0; i < 20; i++) {
                        fib.addNext();
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                };
        Runnable run2 = () -> {
            for (int i = 0; i < 20; i++) {
                fib.print();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("");
            }

        };

        ExecutorService executor = Executors.newFixedThreadPool(2);
        executor.submit(run1);
        executor.submit(run2);
        executor.shutdown();
    }
}
