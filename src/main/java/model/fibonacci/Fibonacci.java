package model.fibonacci;

public class Fibonacci {
    int[] row;
    int n;

    public Fibonacci(int n) {
        this.n = n;
        row = init(n);

    }

    private int[] init(int n) {
        int[] fib = new int[n];
        fib[0] = 1;
        fib[1] = 1;
        for (int i = 2; i < n; i++) {
            fib[i] = fib[i - 1] + fib[i - 2];
        }
        return fib;
    }

    public int addNext() {
        row = init(++n);
        return getRow()[n - 1];
    }


    public int[] getRow() {
        return row;
    }

    public void setRow(int[] row) {
        this.row = row;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public void print() {
        for (int i = 0; i < getRow().length; i++) {
            System.out.print(getRow()[i] + "; ");
        }
    }
}
