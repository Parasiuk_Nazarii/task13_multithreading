package model.fibonacci;

public class FibonacciThreads {
    private Fibonacci fib = new Fibonacci(10);
    Runnable run1 =
            () -> {
                for (int i = 0; i < 20; i++) {
                    fib.addNext();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };

    Thread t1 = new Thread(run1);
    Thread t2 = new Thread(() -> {
        for (int i = 0; i < 20; i++) {
            fib.print();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("");
        }

    }, "print");

    public void run() {
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
