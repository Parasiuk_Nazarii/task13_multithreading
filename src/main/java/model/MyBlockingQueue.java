package model;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class MyBlockingQueue {
    BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(5);
    String[] greetings = {"Hi", "Aloha", "Hello", "Привіт", "Hola", "Ciao"};
    Thread thread1 = new Thread(
            () -> {
                for (String greeting : greetings) {
                    try {
                        blockingQueue.put(greeting);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Say: " + greeting);
                }
            }
    );
    Thread thread2 = new Thread(
            () -> {
                String greeting = "";
                for (int i = 0; i < 10; i++) {
                    try {
                        greeting = blockingQueue.take();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Get answer: " + greeting);
                }
            }
    );

    public void run() {
        thread1.start();
        thread2.start();
    }

}
