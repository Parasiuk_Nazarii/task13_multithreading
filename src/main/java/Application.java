import view.MyView;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class Application {
    public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
        new MyView().show();

    }
}
