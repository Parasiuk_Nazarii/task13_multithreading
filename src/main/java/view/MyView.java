package view;

import model.MyBlockingQueue;
import model.MyPipe;
import model.PingPong;
import model.SleepAndRun;
import model.fibonacci.FibonacciCallable;
import model.fibonacci.FibonacciExecutor;
import model.fibonacci.FibonacciThreads;
import model.threeThreads.ThreeThreads;
import model.threeThreads.ThreeThreadsLock;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Demonstrate PingPong");
        menu.put("2", "  2 - Fibonacci threads");
        menu.put("3", "  3 - Executor in action");
        menu.put("4", "  4 - Callable in action");
        menu.put("5", "  5 - Sleep and run");
        menu.put("6", "  6 - MyPipe run");
        menu.put("7", "  7 - Three threads synchronized and not");
        menu.put("8", "  8 - Three threads synchronized and not with Lock");
        menu.put("9", "  9 - MyBlockingQueue");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton6);
        methodsMenu.put("8", this::pressButton6);
        methodsMenu.put("9", this::pressButton6);
    }

    private void pressButton1() {
        PingPong.start();
    }

    private void pressButton2() {
        FibonacciThreads ft = new FibonacciThreads();
        ft.run();
    }

    private void pressButton3() {
        FibonacciExecutor fe = new FibonacciExecutor();
        fe.go();
    }

    private void pressButton4() throws InterruptedException {
        FibonacciCallable fc = new FibonacciCallable();
        fc.go();
    }

    private void pressButton5() {
        new SleepAndRun().start();

    }

    private void pressButton6() throws IOException {
        new MyPipe().run();
    }

    private void pressButton7() {
        new ThreeThreads().runAsync();
        new ThreeThreads().runSync();

    }

    private void pressButton8() {
        new ThreeThreadsLock().runAsync();
        new ThreeThreadsLock().runSync();
    }

    private void pressButton9() {
        new MyBlockingQueue().run();
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
